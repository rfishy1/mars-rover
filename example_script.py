# -*- coding: utf-8 -*-
"""
Created on Sun Jan 27 15:45:58 2019

@author: rfishy1
"""

# Example script to run the code 

import os, sys
CURRENT_DIR = os.path.dirname(os.path.abspath(__file__))
sys.path.append(os.path.dirname(CURRENT_DIR))

from src.mars_rover_controller import MarsRoverController

mr_controller = MarsRoverController()

mr_controller.find_final_location('8 10\n1 2 E\nMMLMRMMRRMML')

