# -*- coding: utf-8 -*-
"""
Created on Sat Jan 26 08:22:50 2019

@author: rfishy1
"""

import parse
import numpy as np

class MarsRoverController:
    
    # TODO: should this be a method, or rather a function?
    def parse_string(self, input_string):
        '''
        Converts an input string of format 
        %(x_max)d %(y_max)d\n %(x_init)d %(y_init)d %(orientation)c\n%(execution_list)s"
        to a dictionary containing:
            p_max = [x_max, y_max]
            p_init = [x_init, y_init]
            orientation
            execution_list
        '''
        
        # Define additional types for categorical formats
        def orientation_parser(string):
            if not any(string==x for x in ["N","E","S","W"]):
                raise Exception("Format string did not match. Orientation must be N, E, S or W")
            return string
        
        def execution_list_parser(string):
            # This will also find if spaces are included in the execution_list, since parser is greedy:
            # see https://github.com/r1chardj0n3s/parse#potential-gotchas 
            for a in string:
                if not any(a==x for x in ["M","L","R"]):
                    raise Exception("Format string did not match. Execution_list must be a string containing only M, L and R")
            return string

        
        format_string =  '{:d} {:d}\n{:d} {:d} {:0orientation}\n{:execution_list}'
        res = parse.parse(format_string, input_string, {"orientation":orientation_parser,"execution_list":execution_list_parser})
        
        if res is None:
            raise Exception("Format string did not match")
        
        x_max, y_max, x_init, y_init, orientation, execution_list = res.fixed

        # Ensure that orientation is either N, S, E or W and that execution_list is only M, R and
        
        info_dict = {"p_max": [x_max, y_max],"p_init":[x_init, y_init],"orientation":orientation,"execution_list":execution_list}
        return info_dict
    
    def calculate_location(self, info_dict):
        '''
        Calculates the final rover position given an input dict of the form:
            p_max = [x_max, y_max]
            p_init = [x_init, y_init]
            orientation
            execution_list
            
            This is the form returned by parse_string
            
            If the rover leaves the safe area, and error is thrown
            
        Approach:   
            Iterate through the list, replacing Ms with N, S, E, W, depending 
            on the orientation there, then just sum these up for all the positions.
        '''
                      
        p = np.asmatrix(info_dict["p_init"])
        orientation = info_dict["orientation"]
        p_max = np.asmatrix(info_dict["p_max"])
      
        orientation_dict = {"N":0,"E":1,"S":2,"W":3}    
        initial_orientation = orientation_dict[orientation]
        
        # Replace L with -1, R with +1, M with 0
        execution_list = info_dict["execution_list"]
        num_command = np.zeros([len(execution_list)],dtype=int)
        num_command[[x =="L" for x in execution_list]] = -1
        num_command[[x =="R" for x in execution_list]] = 1
       
        # Orientation is cumsum of this
        all_orientation = np.cumsum(num_command) + initial_orientation
        inv_orientation = {v: k for k, v in orientation_dict.items()}
        final_orientation = inv_orientation[all_orientation[-1]]
        all_orientation = all_orientation[num_command==0]
        
        # Mod this to get the orientation
        all_orientation = np.mod(all_orientation,4)
        
        trans_lookup = np.asarray([np.asarray([0,1]),np.asarray([1,0]),np.asarray([0,-1]),np.asarray([-1,0])])
        
        all_moves = trans_lookup[all_orientation]
        
        all_locations = np.cumsum(all_moves,0) + p
        all_locations = np.concatenate([p, all_locations])
        illegal_idx = np.any([np.any(all_locations > p_max,1),np.any(all_locations < [0, 0],1)],0)
        
        if any(illegal_idx):
            illegal_pos = all_locations[illegal_idx][0]
            raise Exception("You've tried to move outside the safe area at position ({}, {})".format(illegal_pos[0], illegal_pos[1]))

        p = all_locations[-1]
        
        return {"p_final":np.ndarray.tolist(np.squeeze(np.asarray(p))),"orientation":final_orientation}
    
    def find_final_location(self, string):
        final_location = self.calculate_location(self.parse_string(string))
        p_final = final_location["p_final"]
        return_val = "{} {} {}".format(p_final[0], p_final[1], final_location["orientation"])
        print(return_val)
        return return_val
    
    
        
        