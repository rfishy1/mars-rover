# mars-rover

Predicting the final location of a Mars Rover

## Approach

The approach used will be as follows:
1. Start by defining a test. These should cover:
    - Simple cases (moving a few blocks from different starting locations and orientations).
    - Edge cases (starting on the edge of the grid, trying to move off the grid etc).
    - Extreme cases (test long repetitive chains to test performance).
    - Expected failures (starting off the grid, bad input formats etc).
1. Write the code to pass the tests.
    - Focus on simple, working code, regardless of performance.
1. Repeat until all the tests are implemented and pass.
1. Finally, consider whether a faster algorithm exists.

Other considerations:
1. *Accept that I might need to change the requirements and approach as I learn more about the problem.*
1. Use a simple git branching structure (since there should be few conflicts and little parallel development). Use master and dev. Merging up will be done via pull requests, for better transparency.

This approach allows the code to meet the specifications from as early on as possible (as well as ensuring correctness of the later changes), before improvements are made to the algorithm.

## Assumptions and specifications

1. Input format is 
   ```python
   "{x_max}d {y_max}d\n {x_init}d {y_init}d {orientation}c\n{execution_list}s"
   ```
   e.g.
   ```python
   8 10
   1 2 E
   MMLMRMMRRMML
   ```
   or ``"8 10\n1 2 E\nMMLMRMMRRMML"`` as a string  
   where  
   `(x_max,y_max)` is the extent of the safe landing terrain bounds  
   `(x_init, y_init)` is the starting location of the rover  
   `orientation` is the starting orientation (`"N"`,`"E"`, `"S"` or `"W"`)  
   `execution_list` is a string of commands, containing only `"M"`, `"L"` or `"R"`  
1. The rover should not be allowed to leave the grid. If this happens, the program should error. The grid is bordered by ``x = 0``, ``y = 0``, ``x = x_max`` and ``y = y_max``.
1. Inputs not matching the format above should also cause an error to be thrown.

## Instructions for use
An example is given in the ``example_script.py``
1. Import the mars_rover_controller module
1. Instantiate the ``MarsRoverController`` object
1. Call the ``import_final_location`` method

The resulting string will be displayed (and returned by the method). The format is ``"{x_final} {y_final} {orientation}"``.
For the example script, this returns ``3 3 E``

## Other comments
1. This is the first time I've created unit tests in python - it was really great to have the opportunity to learn more about it. 
1. I've never needed multiple folders for the python work I've done before. The construct used in many of the files feels a bit clunky, but works:
   ```python
   import os, sys
   CURRENT_DIR = os.path.dirname(os.path.abspath(__file__))
   sys.path.append(os.path.dirname(CURRENT_DIR))
   ```
1. I'm not sure that the functionality in the MarsRoverController class needs to in a class, since python uses modules to hold related functionality (and there are no shared properties).
1. The calculation used (a for-loop over the commands) produces the correct answer, is intuitive, and supports checking if the rover leaves the grid. However, it's not very efficient. It may be more efficient to convert each command into a matrix (either for rotation or translation) and apply these all at once. The matrix manipulation should be faster than the if statements and indexing into dictionaries inside a for-loop.
    1. **EDIT** - having implemented a matrix based approach, this gives about a 15x speedup, while retaining the same error checking.
    1. Having the testsuite in place makes it very easy to validate that the changes to the algorithm (which affect code-readability) are correct.
