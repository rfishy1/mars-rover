# -*- coding: utf-8 -*-
"""
Created on Sat Jan 26 13:29:05 2019

@author: rfishy1
"""

import unittest

import os, sys
CURRENT_DIR = os.path.dirname(os.path.abspath(__file__))
sys.path.append(os.path.dirname(CURRENT_DIR))

from src.mars_rover_controller import MarsRoverController

class TestLocationPrediction(unittest.TestCase):
    '''
    Tests that the location prediction works as expected
    '''

    def test_simple_locations(self):
        mr_controller = MarsRoverController()
        
        # Test the example
        self.assertEqual(mr_controller.calculate_location( {"p_max": [8, 10],"p_init":[1,2],"orientation":"E","execution_list":"MMLMRMMRRMML"}),
                         {"p_final":[3, 3],"orientation":"S"})
        
        # Other simple tests
        self.assertEqual(mr_controller.calculate_location( {"p_max": [100, 100],"p_init":[0,0],"orientation":"E","execution_list":"MMLLMMRR"}),
                         {"p_final":[0, 0],"orientation":"E"})
        
        self.assertEqual(mr_controller.calculate_location( {"p_max": [8, 10],"p_init":[1,2],"orientation":"E","execution_list":"LMRMLMRM"}),
                         {"p_final":[3, 4],"orientation":"E"})
        
    def test_edge_case(self)        :
        # Test moving off the grid and very long chains of command
        mr_controller = MarsRoverController()
        
        # Try move off the grid
        with self.assertRaises(Exception):
            mr_controller.calculate_location({"p_max": [8, 10],"p_init":[1,2],"orientation":"E","execution_list":"MMMMMMMMMMM"})
            
        with self.assertRaises(Exception):
            mr_controller.calculate_location({"p_max": [8, 10],"p_init":[1,2],"orientation":"N","execution_list":"MMMMMMMMMMM"})
            
        with self.assertRaises(Exception):
            mr_controller.calculate_location({"p_max": [8, 10],"p_init":[1,2],"orientation":"S","execution_list":"MMMMMMMMMMM"})
            
        with self.assertRaises(Exception):
            mr_controller.calculate_location({"p_max": [8, 10],"p_init":[1,2],"orientation":"W","execution_list":"MMMMMMMMMMM"})
            
        # Start off the grid (and try move on straight away)
        with self.assertRaises(Exception):
            mr_controller.calculate_location({"p_max": [8, 10],"p_init":[-1,0],"orientation":"E","execution_list":"MMMMMMMMMMM"})
            
        with self.assertRaises(Exception):
            mr_controller.calculate_location({"p_max": [8, 10],"p_init":[0,-1],"orientation":"N","execution_list":"MMMMMMMMMMM"})
           
        # Test a long cycle
        cycle_length = int(1e6)
        self.assertEqual(mr_controller.calculate_location( {"p_max": [cycle_length, cycle_length],"p_init":[0,0],"orientation":"E","execution_list":cycle_length*"MLMR"}),
                         {"p_final":[cycle_length, cycle_length],"orientation":"E"})   
        
if __name__ == '__main__':
    unittest.main()