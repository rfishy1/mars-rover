# -*- coding: utf-8 -*-
"""
Created on Sun Jan 27 16:13:07 2019

@author: rfishy1
"""

import unittest

import os, sys
CURRENT_DIR = os.path.dirname(os.path.abspath(__file__))
sys.path.append(os.path.dirname(CURRENT_DIR))

from src.mars_rover_controller import MarsRoverController

class TestEndToEnd(unittest.TestCase):
    '''
    Tests a few end-to-end cases
    '''

    def test_end_to_end(self):
        mr_controller = MarsRoverController()
        self.assertEqual(mr_controller.find_final_location('8 10\n1 2 E\nMMLMRMMRRMML'),
                         '3 3 S')
        
        self.assertEqual(mr_controller.find_final_location('100 100\n0 0 E\nMMLLMMRR'),
                        '0 0 E')
        
        self.assertEqual(mr_controller.find_final_location('8 10\n1 2 E\nLMRMLMRM'),
                        '3 4 E')