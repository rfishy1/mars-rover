# -*- coding: utf-8 -*-
"""
Created on Sat Jan 26 08:03:25 2019

@author: rfishy1
"""

import unittest

import os, sys
CURRENT_DIR = os.path.dirname(os.path.abspath(__file__))
sys.path.append(os.path.dirname(CURRENT_DIR))

from src.mars_rover_controller import MarsRoverController

class TestInputParser(unittest.TestCase):
    '''
    Tests that the input parser correctly parses the inputs, or errors where 
    appropriate
    '''

    def test_simple_parsing(self):
        mr_controller = MarsRoverController()
        self.assertEqual(mr_controller.parse_string('8 10\n1 2 E\nMMLMRMMRRMML'),
                         {"p_max": [8, 10],"p_init":[1,2],"orientation":"E","execution_list":"MMLMRMMRRMML"})
        
        # Test larger domains and starting locations
        self.assertEqual(mr_controller.parse_string('1785 925\n42 875 N\nLLMMLM'),
                         {"p_max": [1785, 925],"p_init":[42,875],"orientation":"N","execution_list":"LLMMLM"})
        
        # Test single execution list
        self.assertEqual(mr_controller.parse_string('51 19\n12 12 W\nM'),
                         {"p_max": [51, 19],"p_init":[12,12],"orientation":"W","execution_list":"M"})
        
    def test_bad_inputs(self):
        mr_controller = MarsRoverController()
        #%% Test numeric inputs
        with self.assertRaises(Exception): 
            mr_controller.parse_string('a 10\n1 2 E\nMMLMRMMRRMML')
            
        with self.assertRaises(Exception): 
            mr_controller.parse_string('8 b\n1 2 E\nMMLMRMMRRMML')
            
        with self.assertRaises(Exception): 
            mr_controller.parse_string('8 10\nxyv 2 E\nMMLMRMMRRMML')
            
        with self.assertRaises(Exception): 
            mr_controller.parse_string('8 10\n1 q E\nMMLMRMMRRMML')
            
        with self.assertRaises(Exception): 
            mr_controller.parse_string('8.5 10\n1 2 E\nMMLMRMMRRMML')
            
        #%% Test str inputs
        with self.assertRaises(Exception): 
            mr_controller.parse_string('8 10\n1 2 5\nMMLMRMMRRMML')
            
        with self.assertRaises(Exception): 
            mr_controller.parse_string('8 10\n1 2 E\nMMLMR5MRRMML')
        
        # Test categories (N, E, S, W and M,R,L)
        with self.assertRaises(Exception): 
            mr_controller.parse_string('8 10\n1 2 T\nMMLMRMMRRMML')
            
        with self.assertRaises(Exception): 
            mr_controller.parse_string('8 10\n1 2 F\nMMLMRMMRRMML')
            
        with self.assertRaises(Exception): 
            mr_controller.parse_string('8 10\n1 2 E\nQMLMRMMRRMML')
            
        with self.assertRaises(Exception): 
            mr_controller.parse_string('8 10\n1 2 E\nMMLMRMMZRMML')
            
        with self.assertRaises(Exception): 
            mr_controller.parse_string('8 10\n1 2 E\nA')
            
        #%% Test wrong number of fields
        with self.assertRaises(Exception): 
            mr_controller.parse_string('8\n1 2 E\nMMLMRMMZRMML')
            
        with self.assertRaises(Exception): 
            mr_controller.parse_string('8 10\n1 2 E')
            
        with self.assertRaises(Exception): 
            mr_controller.parse_string('8 10\n1 2 E\nMMLMRMMZRMML 9')
        

if __name__ == '__main__':
    unittest.main()