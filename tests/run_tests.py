# -*- coding: utf-8 -*-
"""
Created on Sat Jan 26 08:15:37 2019

@author: rfishy1
"""

# Runs all tests in this folder

import unittest
loader = unittest.TestLoader()
start_dir = '.'
suite = loader.discover(start_dir)

runner = unittest.TextTestRunner()
runner.run(suite)